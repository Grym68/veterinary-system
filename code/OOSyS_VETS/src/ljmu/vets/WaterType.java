package ljmu.vets;

/**
 * This was used to differentiate between
 * different types of fish, might not be needed anymore
 * 
 * @author Grym68
 *
 */
public enum WaterType {
	SALT, FRESH;
}
