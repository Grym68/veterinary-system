package ljmu.vets.invoice.interfaces;

/**
 * Handles saving the Invoice
 * @author Grym68
 *
 */
public interface ISaveInvoice {
	
	/**
	 * Use this to save Invoice by specifying a path.
	 * @param path
	 */
	public void save(String path);
}