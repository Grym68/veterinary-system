package ljmu.vets.invoice.interfaces;

import java.util.List;

import ljmu.vets.booking.Booking;

/**
 * Enforces standard for implementing a Calculatable
 * and allows decoupling
 *  
 * @author Grym68
 *
 */
public interface Calculatable {

	/**
	 * Use this to calculate the amount to charge for the bookings
	 *  of a pet?
	 * @param bookings - the list of bookings a pet has?
	 * @return - the amount to charge as a 2 decimal place double
	 */
	Double calculateAmount(List<Booking> bookings);

}