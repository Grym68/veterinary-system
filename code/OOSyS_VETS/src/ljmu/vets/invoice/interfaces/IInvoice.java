package ljmu.vets.invoice.interfaces;

import java.time.LocalDateTime;
import java.util.List;

import ljmu.vets.booking.Booking;
import ljmu.vets.payable.Payable;

/**
 * Interface to Wrap and represent an Invoice
 * used for decoupling
 * @author Grym68
 *
 */
public interface IInvoice {

	Integer getNo();

	void setNo(Integer no);

	LocalDateTime getWhen();

	void setWhen(LocalDateTime when);

	Double getAmount();

	void setAmount(Double amount);

	List<Booking> getBookings();

	void setBookings(List<Booking> bookings);

	List<Payable> getPayables();

	void setPayables(List<Payable> payables);

	ISaveInvoice getInvoiceSaver();

	void setInvoiceSaver(ISaveInvoice invoiceSaver);

	Calculatable getCalculatable();

	void setCalculatable(Calculatable calculatable);

}