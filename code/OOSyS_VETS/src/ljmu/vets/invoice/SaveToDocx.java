package ljmu.vets.invoice;

import ljmu.vets.invoice.interfaces.ISaveInvoice;

/**
 * COntains the logic to save an invoice to DOCX
 * @author Grym68
 *
 */
public class SaveToDocx implements ISaveInvoice{
	
	/**
	 * The save method that will implement the saving to DOCX
	 * 
	 * @param path - the path where the invoice should be saved
	 */
	public void save(String path) {
		// save to docx
	}

}
