package ljmu.vets.invoice;

import ljmu.vets.invoice.interfaces.ISaveInvoice;

/**
 * Handles the Saving of Invoice to PDF
 * @author Grym68
 *
 */
public class SaveToPDF implements ISaveInvoice{
		
	/**
	 * Saves the Invoice to PDF, at the specified location, <code>path</code>
	 * 
	 * @Param path - the path where the Invoice should be saved
	 */
	public void save(String path) {
			// save to PDF
	}
	
	
	// Complete the implementations, and possibly overload the implementation
	// above to allow for multiple domains

}
