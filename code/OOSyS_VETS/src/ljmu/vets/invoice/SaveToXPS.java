package ljmu.vets.invoice;

import ljmu.vets.invoice.interfaces.ISaveInvoice;

/**
 * Handles saving an Invoice to XPS
 * @author Grym68
 *
 */
public class SaveToXPS implements ISaveInvoice{
	
	/**
	 * Saves an Invoice to XPS at the specified location <code>path</code>
	 * 
	 * @param path - the location at which to save the file
	 */
	@Override
	public void save(String path) {
		// save to XPS
	}

}
