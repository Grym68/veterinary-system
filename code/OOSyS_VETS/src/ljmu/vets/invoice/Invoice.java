package ljmu.vets.invoice;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import ljmu.vets.booking.Booking;
import ljmu.vets.invoice.interfaces.Calculatable;
import ljmu.vets.invoice.interfaces.IInvoice;
import ljmu.vets.invoice.interfaces.ISaveInvoice;
import ljmu.vets.payable.Payable;

/**
 * The base Class for Invoice meant to be extended to customise
 * the implementation for different Invoices
 * @author Grym68
 *
 */
public abstract class Invoice implements Serializable, IInvoice {
	private Integer no;
	private LocalDateTime when;
	private Double amount;
	private List<Booking> bookings;
	private List<Payable> payables;
	protected ISaveInvoice invoiceSaver;
	protected Calculatable calculatable;
//	protected ISaveInvoice invoiceSaver = new InvoiceSaver(); // these should be removed from here and set in the setter :/
//	protected Calculatable calculatable = new InvoiceCalculator();	

	/**
	 * Constructor for Invoice
	 * 
	 * @param invoiceSaver - the implementation of saving an invoice
	 * @param calculatable - the implementation for calculating Charges
	 */
	public Invoice(ISaveInvoice invoiceSaver, Calculatable calculatable) {
		this.invoiceSaver = invoiceSaver;
		this.calculatable = calculatable;
	}

	
	/*
	 * ########################################
	 * ########## GETTERS AND SETTERS #########
	 * ########################################
	 */
	
	@Override
	public Integer getNo() {
		return no;
	}

	@Override
	public void setNo(Integer no) {
		this.no = no;
	}

	@Override
	public LocalDateTime getWhen() {
		return when;
	}

	@Override
	public void setWhen(LocalDateTime when) {
		this.when = when;
	}

	@Override
	public Double getAmount() {
		return amount;
	}

	@Override
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Override
	public List<Booking> getBookings() {
		return bookings;
	}

	@Override
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public List<Payable> getPayables() {
		return payables;
	}

	@Override
	public void setPayables(List<Payable> payables) {
		this.payables = payables;
	}

	@Override
	public ISaveInvoice getInvoiceSaver() {
		return invoiceSaver;
	}

	@Override
	public void setInvoiceSaver(ISaveInvoice invoiceSaver) {
		this.invoiceSaver = invoiceSaver;
	}

	@Override
	public Calculatable getCalculatable() {
		return calculatable;
	}

	@Override
	public void setCalculatable(Calculatable calculatable) {
		this.calculatable = calculatable;
	}

}
