package ljmu.vets.invoice;

import java.text.DecimalFormat;
import java.util.List;

import ljmu.vets.booking.Booking;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.invoice.interfaces.Calculatable;
import ljmu.vets.pet.cat.Cat;
import ljmu.vets.pet.fish.Fish;

/**
 * Handles all the calculations for an Invoice
 * @author Grym68
 *
 */
public class InvoiceCalculator implements Calculatable {

	/**
	 * Use this to calculate the amount to charge for the bookings of a pet.
	 * It is pet.calculate has a different implementation depending of the type
	 * of pet
	 * 
	 * @param bookings - the list of bookings a pet has?
	 * 
	 * @return - the amount to charge as a 2 decimal place double
	 */
	@Override
	public Double calculateAmount(List<Booking> bookings) {
		Double tt = 0.0;

		for (IBooking o : bookings) {			
			tt += o.getPet().getCalculate().calculate(o.getDuration(), o.getPet());
		}

		return Double.parseDouble(new DecimalFormat("#.##").format(tt));
	}
}
