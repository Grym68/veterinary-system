package ljmu.vets.invoice;

import ljmu.vets.invoice.interfaces.Calculatable;
import ljmu.vets.invoice.interfaces.ISaveInvoice;

/**
 * This implementation of the Invoice will export to Docx
 * 
 * @author Grym68
 *
 */
public class InvoiceToDocx extends Invoice{

	public InvoiceToDocx(ISaveInvoice invoiceSaver, Calculatable calculatable) {
		// TODO Auto-generated constructor stub
		super(invoiceSaver, calculatable);
	}

}
