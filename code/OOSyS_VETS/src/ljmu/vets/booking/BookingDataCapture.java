package ljmu.vets.booking;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingDataCapture;
import ljmu.vets.pet.IPet;

/**
 * This class contains methods used to Capture data about for a Booking
 * 
 * @author Grym68
 *
 */
public class BookingDataCapture implements IBookingDataCapture {
	// all of the occurrences of this scanner throughout the system
	// should be handled by some input handler classes
	Scanner S = new Scanner(System.in);

	/**
	 * Use this to capture the booking reference from user input as a String
	 * 
	 * @return - the booking reference as a String
	 */
	@Override
	public String captureReference() {
		System.out.print("Booking's Ref : ");
		String input = S.nextLine();
		return input;
	}
	
	/**
	 * Use this to capture user input for the date for the booking 
	 * as a LocalDateTime
	 * 
	 * @return - the date of the booking as LocalDateTime 
	 */
	@Override
	public LocalDateTime captureWhen() {
		System.out.print("Booking's DateTime [i.e. 03 Oct 23 09:30] : ");
		LocalDateTime input = LocalDateTime.parse(S.nextLine(),
				DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));
		return input;
	}
	
	/**
	 * Use this to capture user input for the duration of the booking as a
	 * Integer
	 * 
	 * @return - the duration of the booking as a Integer
	 */
	@Override
	public Integer captureDuration() {
		System.out.print("Booking's Duration [i.e. MINS] minimum of 15: ");
		Integer input = Integer.parseInt(S.nextLine());
		return input;
	}
	
	/**
	 * Use this to capture all the data needed for a Booking
	 * and initiate a new Booking object
	 * 
	 * @param pet - the Pet for which the Booking is made
	 * 
	 * @return - a new Booking object initiated with user input
	 */
	@Override
	public IBooking capture(IPet pet) {
		IBooking output = Factory.createBooking(); // DIP
		output.setRef(captureReference());
		output.setWhen(captureWhen());
//		Pet pet = PetGenerator.capture();
		output.setPet(pet);
		output.setDuration(captureDuration());
		return output;
	}
	
	
}
