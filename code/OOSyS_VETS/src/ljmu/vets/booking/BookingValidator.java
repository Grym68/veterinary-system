package ljmu.vets.booking;

import java.time.LocalDateTime;
import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingValidator;

/**
 * Contains methods tha handle validation for a Booking or parts
 * of a Booking
 * 
 * @author Grym68
 *
 */
public class BookingValidator implements IBookingValidator {
	
	/**
	 * Use this to validate the reference of a booking
	 * 
	 * @param reference - the booking reference to check
	 * 
	 * @return - returns true is the reference is valid
	 * or false it is not
	 */
	@Override
	public boolean validateReference(String reference) {
		if(reference.isBlank()) {
			System.err.println(
					"The reference for this booking is blank or empty");
			return false;
		}
		return true;
	}

	/**
	 * Use this to validate if the booking fields are
	 * as expected
	 * 
	 * @param booking - the booking to validate,
	 * 
	 * pass a Booking object to this parameter
	 * @return - true if valid, false otherwise
	 */
	@Override
	public boolean validate(IBooking booking) {
		if (booking.getRef().isBlank()) {
			return false;
		}
		if (booking.getPet() == null) {
			// Show message
			return false;
		}
		if(booking.getWhen().isBefore(LocalDateTime.now())) {
			// Show message
			return false;
		}
		if(booking.getDuration() < 15) {
			// Show message
			return false;
		}
		
		return true;
	}
	
	/**
	 * Use this to validate is the booking is null
	 * 
	 * @param booking - the booking object to check
	 * 
	 * @return - true if it's null, false otherwise
	 */
	@Override
	public boolean isNull(IBooking booking) { // Useful??
		// technically useful because I can throw exceptions here 
		// and catch them at the other end
		
		if (booking == null) {
			System.err.println("The bookings wasn't found");
			return true;
		}
		return false;
	}
	
	/**
	 * Use this to check if a branch already exists in the list of Bookings
	 * 
	 * @param booking - the booking to check
	 * 
	 * @param list - the list to check against
	 * 
	 * @return - true if it does already exist, false otherwise
	 */
	@Override
	public boolean alreadyExists(IBooking booking, List<IBooking> list) {
		for (IBooking iterate: list) {
			if (iterate.getRef() == booking.getRef())
				return true;
		}
		
		return false;
	}
	
}
