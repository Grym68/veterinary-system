package ljmu.vets.booking;

import java.time.format.DateTimeFormatter;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingDataRetriever;

/**
 * This Class contains methods that handle Data Retrieval from a Booking
 * 
 * @author Grym68
 *
 */
public class BookingDataRetrieval implements IBookingDataRetriever {
	
	/**
	 * Use this to get a formated copy of a Booking object as a String
	 * 
	 * @param booking - the booking you want to get
	 * @return - the booking as a String
	 */
	@Override
	public String toString(IBooking booking) {
			return booking.getClass().getSimpleName() + " >> " +
					booking.getRef() + " " +
					booking.getPet().toString() + " " +
					booking.getWhen().format(DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));
		
	}

}
