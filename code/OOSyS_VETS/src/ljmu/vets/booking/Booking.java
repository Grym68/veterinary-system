package ljmu.vets.booking;

import java.io.Serializable;
import java.time.LocalDateTime;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;

/**
 * Represents a Booking for a pet, use this object to 
 * represent a booking for a pet
 * 
 * @author Grym68
 *
 */
public class Booking implements Serializable, IBooking {
	private String ref;
	private IPet pet;
	private LocalDateTime when;
	private Integer duration;

	/**
	 * Construct a new booking by providing
	 * @param ref - the Booking reference as a <code>String</code>
	 * @param pet - the Pet as a <code>Pet Object</code>
	 * @param when - the Date and Time of the Booking as a
	 * <code>LocalDateTime</code>
	 * @param duration - the duration of the Booking as a 
	 * <code>Integer</code>
	 */
//	public Booking(String ref, Pet pet, LocalDateTime when, Integer duration) {
//		this.ref = ref;
//		this.pet = pet;
//		this.when = when;
//		this.duration = duration;
//	}
	
	public Booking() {
		
	}

	
	/*
	 * ########################################
	 * ########## GETTERS AND SETTERS #########
	 * ########################################
	 */
	@Override
	public String getRef() {
		return this.ref;
	}

	@Override
	public IPet getPet() {
		return this.pet;
	}

	@Override
	public LocalDateTime getWhen() {
		return this.when;
	}

	@Override
	public Integer getDuration() {
		return this.duration;
	}

	@Override
	public void setRef(String ref) {
		this.ref = ref;
	}

	@Override
	public void setPet(IPet pet) {
		this.pet = pet;
	}

	@Override
	public void setWhen(LocalDateTime when) {
		this.when = when;
	}

	@Override
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	// ToDo : get / set Methods ?
}
