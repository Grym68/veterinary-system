package ljmu.vets.booking.interfaces;

/**
 * Contains all of the methods needed by the Data Retrieval for Booking
 * Used for decoupling
 * @author Grym68
 *
 */
public interface IBookingDataRetriever {

	/**
	 * Use this to get a formated copy of a Booking object as a String
	 * @param booking - the booking you want to get
	 * @return - the booking as a String
	 */
	String toString(IBooking booking);

}