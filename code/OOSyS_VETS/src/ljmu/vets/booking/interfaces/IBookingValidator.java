package ljmu.vets.booking.interfaces;

import java.util.List;

/**
 * Interface containing all of the methods needed for 
 * Validating Bookings, help enforce standards and decouple
 * @author Grym68
 *
 */
public interface IBookingValidator {

	/**
	 * Use this to validate the reference of a booking
	 * @param reference - the booking reference to check
	 * @return - returns true is the reference is valid
	 * or fals it is not
	 */
	boolean validateReference(String reference);

	/**
	 * Use this to validate if the booking fields are
	 * as expected
	 * @param booking - the booking to validate,
	 * pass a Booking object to this parameter
	 * @return - true if valid, false otherwise
	 */
	boolean validate(IBooking booking);

	/**
	 * Use this to validate is the booking is null
	 * Useful??
	 * @param booking - the booking object to check
	 * @return - true if it's null, false otherwise
	 */
	boolean isNull(IBooking booking);

	/**
	 * Use this to check if a branch already exists in the list of Bookings
	 * @param booking - the booking to check
	 * @param list - the list to check against
	 * @return - true if it does already exist, false otherwise
	 */
	boolean alreadyExists(IBooking booking, List<IBooking> list);

}