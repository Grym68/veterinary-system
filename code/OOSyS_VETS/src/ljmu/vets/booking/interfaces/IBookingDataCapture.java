package ljmu.vets.booking.interfaces;

import java.time.LocalDateTime;

import ljmu.vets.pet.IPet;

/**
 * Interface containing all of the methods a BookingDataCapture should contain
 * @author Grym68
 *
 */
public interface IBookingDataCapture {

	/**
	 * Use this to capture the booking reference from user input as a String
	 * @return - the booking reference as a String
	 */
	String captureReference();

	/**
	 * Use this to capture user input for the date for the booking 
	 * as a LocalDateTime
	 * @return - the date of the booking as LocalDateTime 
	 */
	LocalDateTime captureWhen();

	/**
	 * Use this to capture user input for the duration of the booking as a
	 * Integer
	 * @return - the duration of the booking as a Integer
	 */
	Integer captureDuration();

	/**
	 * Use this to capture all the data needed for a Booking
	 * and initiate a new Booking object
	 * @param pet - the Pet for which the Booking is made
	 * @return - a new Booking object initiated with user input
	 */
	IBooking capture(IPet pet);

}