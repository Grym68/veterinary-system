package ljmu.vets.booking.interfaces;

import java.time.LocalDateTime;

import ljmu.vets.pet.IPet;

/**
 * Interface for standardising Booking and decoupling
 * @author Grym68
 *
 */
public interface IBooking {

	/*
	 * ########################################
	 * ########## GETTERS AND SETTERS #########
	 * ########################################
	 */
	String getRef();

	IPet getPet();

	LocalDateTime getWhen();

	Integer getDuration();

	void setRef(String ref);

	void setPet(IPet pet);

	void setWhen(LocalDateTime when);

	void setDuration(Integer duration);

}