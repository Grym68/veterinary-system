package ljmu.vets.payable;

/**
 * Implement this interface when only the 
 * Surgery Payable methods are needed
 * @author Grym68
 *
 */
public interface SurgeryPayable {

	/**
	 * Use this to get the current public Cost
	 * @return - returns the Public cost as a double
	 */
	Double getSurgeryCost();

}