package ljmu.vets.payable;

/**
 * Implement this interface when both the 
 * surgery and public payable methods are needed
 * 
 * @author Grym68
 *
 */
public interface Payable extends SurgeryPayable, PublicPayable {
	
}
