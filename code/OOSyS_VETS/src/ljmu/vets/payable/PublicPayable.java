package ljmu.vets.payable;

/**
 * Implement this interface when only 
 * the Public payable methods are needed
 * @author Grym68
 *
 */
public interface PublicPayable {

	/**
	 * Use this to get the current Surgery Cost
	 * @return - the surgery cost as a double
	 */
	Double getPublicCost();

}