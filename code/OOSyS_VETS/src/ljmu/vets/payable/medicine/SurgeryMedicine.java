package ljmu.vets.payable.medicine;

import java.io.Serializable;

import ljmu.vets.notifiable.Notifiable;
import ljmu.vets.payable.SurgeryPayable;
import ljmu.vets.payable.medicine.interfaces.ISurgeryMedicine;

/**
 * Object representing the Medicine a Surgery has
 * 
 * @author Grym68
 *
 */
public class SurgeryMedicine extends Medicine implements SurgeryPayable, Serializable, ISurgeryMedicine {
	private Double surgeryCost;

	public SurgeryMedicine() {
		super();
	}

@Override
	public void setStock(Integer i) {
		this.stock = i;

		if (i < lowest) {
			notifyPeople();
		}
	}

//	@Override
//	public void addNotifiable(Notifiable n) {
//		this.people.add(n);
//	}

	// this could be SRP'ed
	protected void notifyPeople() {
		for	(Notifiable n : people) {
			n.notify(this.stock + " of " + this.name);
		}
	}


	@Override
	public Double getSurgeryCost() {
		return this.surgeryCost;
	}
}
