package ljmu.vets.payable.medicine;

import java.io.Serializable;

import ljmu.vets.payable.PublicPayable;
import ljmu.vets.payable.medicine.interfaces.IPublicMedicine;

/**
 * Represents the Medicine of the Public
 * not very clear what is meant by this.
 * This part of the code was quite cryptic and
 * this is as far as I managed to make sense of it so far
 * 
 * @author Grym68
 * @author Dr. Glyn Hughes
 *
 */
public class PublicMedicine extends Medicine implements PublicPayable, Serializable, IPublicMedicine{ // this should implement a smaller part of payable
	private Double publicCost;

	public PublicMedicine() {
		super();
	}

	@Override
	public void setStock(Integer i) {
		this.stock = i;
	}

	@Override
	public Double getPublicCost() {
		return this.publicCost;
	}

	// ToDo : get / set Methods ?
}
