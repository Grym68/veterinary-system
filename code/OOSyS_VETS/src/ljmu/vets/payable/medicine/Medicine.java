package ljmu.vets.payable.medicine;

import java.util.ArrayList;
import java.util.List;

import ljmu.vets.notifiable.Notifiable;
import ljmu.vets.payable.medicine.interfaces.IMedicine;

/**
 * A Medicine object that would contain all 
 * the fields and methods needed for a Medicine object
 *  
 * @author Grym68
 *
 */
public class Medicine implements IMedicine {

	protected String name;
	protected Integer stock;
	protected Integer lowest;
	protected List<Notifiable> people = new ArrayList<Notifiable>(); // this might violate OLD principles
	
	/**
	 * Constructor
	 */
	public Medicine() {

	}
	


//
//	// If implemented it violated SRP, I will neeed to move this to a separete class
//	protected void notifyPeople() {
//		for	(Notifiable n : people) { // What??? this does nothing :/
//			n.notify(this.stock + " of " + this.name);
//		}
//	}
	
	
	// ###########################
	// ### Getters and Setters ###
	// ###########################
	
	@Override
	public void setStock(Integer i) {
		this.stock = i;
	}
	
	@Override
	public int getStock() {
		return this.stock;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Integer getLowest() {
		return lowest;
	}

	@Override
	public void setLowest(Integer lowest) {
		this.lowest = lowest;
	}

	@Override
	public List<Notifiable> getPeople() {
		return people;
	}

	@Override
	public void setPeople(List<Notifiable> people) {
		this.people = people;
	}
	
	
}
