package ljmu.vets.payable.medicine.interfaces;

import java.util.List;

import ljmu.vets.notifiable.Notifiable;

public interface IMedicine {

	void setStock(Integer i);

	int getStock();

	String getName();

	void setName(String name);

	Integer getLowest();

	void setLowest(Integer lowest);

	List<Notifiable> getPeople();

	void setPeople(List<Notifiable> people);

}