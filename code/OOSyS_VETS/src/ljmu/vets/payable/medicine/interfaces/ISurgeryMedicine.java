package ljmu.vets.payable.medicine.interfaces;

import ljmu.vets.notifiable.Notifiable;

/**
 * Use this interface to implement the specific methods
 * needed by the Surgery Medicine object
 * 
 * @author Grym68
 *
 */
public interface ISurgeryMedicine {

	/**
	 * @return - the costs for this surgery
	 */
	Double getSurgeryCost();

}