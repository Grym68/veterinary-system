package ljmu.vets.payable.medicine.interfaces;

/**
 * Use this interface to implement the methods
 * required by the public medicine class
 * @author Grym68
 *
 */
public interface IPublicMedicine {

	/**
	 * 
	 * @return - return the public costs for the
	 * Public Medicine
	 */
	Double getPublicCost();

}