package ljmu.vets.payable.medicine;

import ljmu.vets.notifiable.Notifiable;
import ljmu.vets.payable.medicine.interfaces.IMedicine;

/**
 * Handles the Generation of Medicines or
 * parts of the medicine Objects
 * @author Grym68
 *
 */
public class MedicineGenerator {
	
	/**
	 * Use this to add a notifiable to the list of Notifiables in the
	 * Medicine Object by passing in a medicine object and a notifiable object
	 * 
	 * @param medicine - the medicine object to update
	 * @param notifiable - the notifiable to be added to the medicine object
	 * @return - the updated medicine object
	 */
	public IMedicine addNotifiable(IMedicine medicine , Notifiable notifiable) {
		medicine.getPeople().add(notifiable);
		return medicine;
	}

}
