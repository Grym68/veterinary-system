package ljmu.vets;

/**
 * These Enums items are used for saving the invoices to
 * different file formats
 * 
 * @author Grym68
 *
 */
public enum FileType {
	DOCX, XPS, PDF;
}
