package ljmu.vets;

import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.sys.Authenticate;
import ljmu.vets.sys.SurgeryMenus;
import ljmu.vets.sys.Sys;
import ljmu.vets.sys.SysGenerator;

/**
 * This is the Entry Menu for the user of the system,
 * it can be swapped for a GUI Menu
 * @author Grym68
 *
 */
public class EntryMenus {
Scanner S = new Scanner(System.in);

 	/**
 	 * The entry menu method, this is the actual entry menu,
 	 * it allows the user to navigate the system before logging in
 	 * @param system - the Type of system it is passed to it,
 	 *  this could be a concreation :?
 	 */
	public void entryMenu(Sys system) {
		String choice = "";

		do {
			System.out.println("-- ENTRY MENU --");
			System.out.println("1 : [L]ogOn");
			System.out.println("Q : Quit");
			System.out.print("Pick : ");

			choice = S.nextLine().toUpperCase();

			switch (choice) {
			case "1":
			case "L": {
				system.setSurgery(Factory.surgeryMenus().surgeryMenu(
										Factory.getAuthenticate().logOn(system.getSurgeries())));
				
				system.setSurgeries(Factory.getSysGenerator().updateSurgeries(
								system.getSurgery(), system.getSurgeries()));
				
				Factory.getSerializer().serialize(
						system.getSurgeries(), system.getPATH());
				break;
			}
			}

		} while (!choice.equals("Q"));
		
		Factory.getSerializer().serialize(
				Factory.getSysGenerator().updateSurgeries(
						system.getSurgery(), system.getSurgeries()),
				system.getPATH());
		
		System.out.println("Bye Bye !");

		System.exit(0);
	}
}
