package ljmu.vets.pet;

import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.pet.interfaces.IMamal;

public class PetGenerator {
	// TODO: maybe implement the classes used in the previous CW to use input for the scanner
	Scanner S = new Scanner(System.in);
	PetDataCapture petCapturor = new PetDataCapture();
	
	/**
	 * Use this to make a  <code>Pet Object</code>
	 */
	public IPet generate() { // this is broken too
		System.out.println("Choose what type of pet to add:\n"
				+ "1 : Fish\n"
				+ "2 : Cat");
		String choice = S.nextLine();
		
		switch (choice) {
			case "1" : {
			// this violates OCP and DIP probably but I'm not sure how to fix it
				IAquatic output = Factory.createFish();	
				return output.getPetCapture().capture();
				}
			case "2" : {
				// this violates DIP and probably OCP, but I'm not sure how to fix it
				IMamal output = Factory.createCat();
				return output.getPetCapture().capture();
			}
			default:
				System.err.println("Invalid Option!");
		}
		
		return petCapturor.capture();
	}
	

	/**
	 * Use this to add a new Booking to Pet
	 * @param pet - the pet to add the booking to as an instance of Pet
	 * @param iBooking - the booking to add to the pet as an instance of Booking
	 * @return - returns the updated pet object
	 */
	public static IPet addBookingToPet(IPet pet, IBooking iBooking) { // possibly add Booking as a parameter.
		
	//		Booking booking = BookingDataCapture.capture(pet);
		pet.getBookings().add(iBooking);
		return pet;
	}
}
