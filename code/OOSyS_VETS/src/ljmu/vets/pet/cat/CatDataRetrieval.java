package ljmu.vets.pet.cat;

import java.time.format.DateTimeFormatter;

import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.pet.interfaces.IMamal;
import ljmu.vets.retrieve.IPetDataRetrieval;

public class CatDataRetrieval implements IPetDataRetrieval<IMamal>{

	@Override
	public String toString(IMamal cat) {
		return cat.getClass().getSimpleName() + " >> " +
				cat.getName() + " " +
				cat.getRegDate().format(DateTimeFormatter.ofPattern("dd MMM yy"));
	}
}
