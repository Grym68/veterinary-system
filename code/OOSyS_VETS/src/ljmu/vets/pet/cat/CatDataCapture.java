package ljmu.vets.pet.cat;

import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.interfaces.IPetCapture;

public class CatDataCapture implements IPetCapture {
	static Scanner S = new Scanner(System.in);

	/**
	 * Use this to capture the data for a Cat
	 * @return - a Cat Object
	 */
	@Override
	public IPet capture() {
		IPet output = Factory.createCat();
		output.setName(output.getDataCapture().captureName());
		output.setRegDate(output.getDataCapture().captureRegistrationDate());
		return output;
	}
}
