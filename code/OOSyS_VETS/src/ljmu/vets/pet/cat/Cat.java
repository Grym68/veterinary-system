package ljmu.vets.pet.cat;

import ljmu.vets.pet.IPetDataCapture;
import ljmu.vets.pet.Pet;
import ljmu.vets.pet.calculate.CalculateCat;
import ljmu.vets.pet.calculate.ICalculate;
import ljmu.vets.pet.interfaces.IMamal;
import ljmu.vets.pet.interfaces.IPetCapture;
import ljmu.vets.retrieve.IPetDataRetrieval;

public class Cat extends Pet implements IMamal{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IPetDataRetrieval dataRetrieval;
	private IPetDataCapture dataCapture;
	public IPetCapture petCapture;
	public ICalculate calculate;
	
	public Cat() {
		super();
		this.petCapture = new CatDataCapture();
		this.calculate = new CalculateCat();
		
	}
	
	@Override
	public IPetCapture getPetCapture() {
		// TODO Auto-generated method stub
		return petCapture;
	}

	@Override
	public void setPetCapture(IPetCapture petCapture) {
		// TODO Auto-generated method stub
		this.petCapture = petCapture;
		
	}


	// ToDo : get / set Methods ?
}
