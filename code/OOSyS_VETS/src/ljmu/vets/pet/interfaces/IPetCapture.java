package ljmu.vets.pet.interfaces;

import ljmu.vets.pet.IPet;

public interface IPetCapture {

	/**
	 * Use this to capture the data for a Cat
	 * @return - a Cat Object
	 */
	IPet capture();

}