package ljmu.vets.pet.interfaces;

public interface ICapturatable {

	IPetCapture getPetCapture();
	void setPetCapture(IPetCapture petCapture);
}
