package ljmu.vets.pet.interfaces;

import ljmu.vets.WaterType;
import ljmu.vets.pet.IPet;

public interface IAquatic extends IPet, ICapturatable{

	WaterType getWaterType();

	void setWaterType(WaterType waterType);

}