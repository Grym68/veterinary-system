package ljmu.vets.pet;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.calculate.CalculatePet;
import ljmu.vets.pet.calculate.ICalculate;
import ljmu.vets.retrieve.IPetDataRetrieval;
import ljmu.vets.retrieve.PetDataRetrieval;
import ljmu.vets.retrieve.lists.IRetrieveBookings;

public abstract class Pet implements Serializable, IPet {

	protected String name;
	protected LocalDate regDate;

	private List<IBooking> bookings;
	private IPetDataRetrieval<IPet> dataRetrieval;//OCP
	private IPetDataCapture dataCapture; // remove DIP
	private ICalculate calculate; // OCP
	private IRetrieveBookings bookingsRetriever;
	
	public Pet() { // all useless :))
		this.bookings = new ArrayList<IBooking>();
		this.dataRetrieval = new PetDataRetrieval();
		this.dataCapture = new PetDataCapture();
		this.calculate = new CalculatePet();
		this.bookingsRetriever = new PetDataRetrieval();
		 // empty
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public LocalDate getRegDate() {
		return regDate;
	}

	@Override
	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}

	@Override
	public List<IBooking> getBookings() {
		return bookings;
	}

	@Override
	public void setBookings(List<IBooking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public  IPetDataRetrieval<IPet> getDataRetrieval() {
		return dataRetrieval;
	}

	@Override
	public void setDataRetrieval(IPetDataRetrieval<IPet> dataRetrieval) {
		this.dataRetrieval = dataRetrieval;
	}

	@Override
	public IPetDataCapture getDataCapture() {
		return dataCapture;
	}

	@Override
	public void setDataCapture(IPetDataCapture dataCapture) {
		this.dataCapture = dataCapture;
	}

	@Override
	public ICalculate getCalculate() {
		return calculate;
	}

	@Override
	public void setCalculate(ICalculate calculate) {
		this.calculate = calculate;
	}

	@Override
	public IRetrieveBookings getBookingsRetriever() {
		return bookingsRetriever;
	}

	@Override
	public void setBookingsRetriever(IRetrieveBookings bookingsRetriever) {
		this.bookingsRetriever = bookingsRetriever;
	}
	
	/*
	 * ########################################
	 * ########## GETTERS AND SETTERS #########
	 * ########################################
	 */
}
