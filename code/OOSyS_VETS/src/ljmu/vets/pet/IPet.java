package ljmu.vets.pet;

import java.time.LocalDate;
import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.calculate.ICalculate;
import ljmu.vets.retrieve.IPetDataRetrieval;
import ljmu.vets.retrieve.lists.IRetrieveBookings;

public interface IPet {

	String getName();

	void setName(String name);

	LocalDate getRegDate();

	void setRegDate(LocalDate regDate);

	List<IBooking> getBookings();

	void setBookings(List<IBooking> bookings);

	IPetDataRetrieval<IPet> getDataRetrieval();

	void setDataRetrieval(IPetDataRetrieval<IPet> dataRetrieval);

	IPetDataCapture getDataCapture();

	void setDataCapture(IPetDataCapture dataCapture);

	ICalculate getCalculate();

	void setCalculate(ICalculate calculate);

	IRetrieveBookings getBookingsRetriever();

	void setBookingsRetriever(IRetrieveBookings bookingsRetriever);

}