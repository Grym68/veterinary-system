package ljmu.vets.pet.calculate;

import ljmu.vets.pet.IPet;

public class CalculateFish implements ICalculate{

	@Override
	public Double calculate(int duration, IPet pet) {
		return duration * 17.0;
	}

}
