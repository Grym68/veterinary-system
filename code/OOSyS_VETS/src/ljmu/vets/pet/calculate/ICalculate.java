package ljmu.vets.pet.calculate;

import ljmu.vets.pet.IPet;

public interface ICalculate {

	/**
	 * calculate the amount for a pet type Cat
	 * @param duration - the duration to multiply by
	 * @param pet - the pet to get the type
	 * @return - the amount as a double
	 */
	Double calculate(int duration, IPet pet);

}