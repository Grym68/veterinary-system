package ljmu.vets.pet.calculate;

import ljmu.vets.pet.IPet;

public class CalculateCat implements ICalculate {

	/**
	 * calculate the amount for a pet type Cat
	 * @param duration - the duration to multiply by
	 * @param pet - the pet to get the type
	 * @return - the amount as a double
	 */
	@Override
	public Double calculate(int duration, IPet pet) {
		return duration * 13.0;
	}
}
