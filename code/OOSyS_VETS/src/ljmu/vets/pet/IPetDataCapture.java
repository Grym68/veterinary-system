package ljmu.vets.pet;

import java.time.LocalDate;

public interface IPetDataCapture {

	/**
	 * Use this to Capture the name of a pet 
	 * @return - the name as a <code>String</code>
	 */
	String captureName();

	/**
	 * Use this to capture the registration date for a pet
	 * @return - the registration data as a LocalDate
	 */
	LocalDate captureRegistrationDate();

}