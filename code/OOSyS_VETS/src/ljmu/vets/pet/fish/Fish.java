package ljmu.vets.pet.fish;

import ljmu.vets.WaterType;
import ljmu.vets.pet.IPetDataCapture;
import ljmu.vets.pet.Pet;
import ljmu.vets.pet.calculate.CalculateFish;
import ljmu.vets.pet.calculate.ICalculate;
import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.pet.interfaces.ICapturatable;
import ljmu.vets.pet.interfaces.IPetCapture;
import ljmu.vets.retrieve.IPetDataRetrieval;

public class Fish extends Pet implements IAquatic,ICapturatable {
	
	private WaterType waterType;
	private IPetDataRetrieval<IAquatic> dataRetrieval;
	private IPetDataCapture dataCapture;
	private IPetCapture petCapture;
	private ICalculate calculate;
	
	public Fish() {
		super();
		this.dataRetrieval = new FishDataRetrieval();
		this.petCapture = new FishDataCapture();
		this.calculate  = new CalculateFish();
	}

	@Override
	public WaterType getWaterType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setWaterType(WaterType waterType) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public IPetCapture getPetCapture() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPetCapture(IPetCapture petCapture) {
		// TODO Auto-generated method stub
		
	}
	
}
