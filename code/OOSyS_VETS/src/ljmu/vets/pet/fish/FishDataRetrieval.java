package ljmu.vets.pet.fish;

import java.time.format.DateTimeFormatter;

import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.retrieve.IPetDataRetrieval;

public class FishDataRetrieval implements IPetDataRetrieval<IAquatic> {

	@Override
	public String toString(IAquatic fish) {
		return fish.getClass().getSimpleName() + " >> " +
				fish.getName() + " " +
				fish.getWaterType().toString() + " " +
				fish.getRegDate().format(DateTimeFormatter.ofPattern("dd MMM yy"));
	}
}
