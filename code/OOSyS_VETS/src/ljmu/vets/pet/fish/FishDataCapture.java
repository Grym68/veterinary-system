package ljmu.vets.pet.fish;

import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.WaterType;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.pet.interfaces.IPetCapture;

public class FishDataCapture implements IPetCapture{
Scanner S = new Scanner(System.in);
	
	
	

	/**
	 * Use this to capture the data for a <code>Fish Object</code>
	 * @return - a populated Fish object
	 */
	@Override
	public IPet capture() {
		IAquatic output = Factory.createFish();
		output.setName(output.getDataCapture().captureName());
		output.setRegDate(output.getDataCapture().captureRegistrationDate());
		output.setWaterType(captureWaterType());
		return output;
	}
	
	/**
	 * Use this to capture WaterType data. It does this by making the user
	 *  pick a WaterType
	 * @return - a waterType
	 */
	public WaterType captureWaterType() {
		WaterType output = WaterType.FRESH;
		System.out.print("Fish's water type: " + "1 : Fresh" + "2 : Salt");
		String choice = S.nextLine();
		switch (choice) {
			case "1": {
				output = WaterType.FRESH;
				break;
			}
			case "2": {
				output = WaterType.SALT;
				break;
			}
			default: {
				System.err.println("Choice Invalid");
			}
		}
		return output;
	}

	
	
}
