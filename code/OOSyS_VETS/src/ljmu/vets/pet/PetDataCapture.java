package ljmu.vets.pet;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import ljmu.vets.pet.fish.Fish;
import ljmu.vets.pet.interfaces.IPetCapture;

public class PetDataCapture implements IPetDataCapture, IPetCapture {
protected Scanner S = new Scanner(System.in);

	/**
	 * Use this to Capture the name of a pet [or any String input that doesn't need validation ;)]
	 * @return - the name as a <code>String</code>
	 */
	@Override
	public String captureName() {
		System.out.print("Pet's Name : ");
		String output = S.nextLine();
		return output;
	}
	
	/**
	 * Use this to capture the registration date for a pet
	 * @return - the registration data as a LocalDate
	 */
	@Override
	public LocalDate captureRegistrationDate() {
		System.out.print("Pet's RegDate [i.e. 03 Oct 23] : ");
		LocalDate output = LocalDate.parse(S.nextLine(), DateTimeFormatter.ofPattern("dd MMM yy"));
		return output;
	}
	
	@Override
	public IPet capture() {
		IPet pet = new Fish();
		pet.setName(captureName());
		pet.setRegDate(captureRegistrationDate());
		return pet;
	}
	
	
}