package ljmu.vets.IO;

import java.util.List;

/**
 * Interface to generalise the methods that should be implemented by
 * a deserializers
 * 
 * @author Grym68
 *
 */
public interface IDeserialize {

	/**
	 * Use this to deserialize data saved at a specified location
	 * as a List by passing the path to the location where the
	 * data is saved to the path parameter
	 * @param <T> - the type of object the list will hold
	 * @param path - the path to the data
	 * @return - returns a list of object <T>
	 */
	<T> List<T> deSerialize(String path);

}