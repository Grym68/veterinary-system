package ljmu.vets.IO;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Handles the implementation of serializing with ObjectSerializer
 * 
 * @author Grym68
 *
 */
public class SerializeWithObjectSerializer implements ISerialize {
	
	/**
	 * Use this to serialize a list of objects type <T>  at a root
	 * location passed to the path parameter
	 * @param <T> - Generics for the type of object the list array
	 * stores
	 * @param array - the array of objects type <T> to serialize
	 * at path
	 */
	@Override
	public <T> void serialize(List<T> array, String path) {
		
		ObjectOutputStream oos;

		try {
			oos = new ObjectOutputStream(new FileOutputStream(path + "surgeries.ser"));
			oos.writeObject(array);

			// ToDo : Finally ?
			oos.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
