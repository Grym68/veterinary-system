package ljmu.vets.IO;

import java.util.List;

/**
 * Interface to map and generalise the methods that need to be
 * implemented in the Serializers
 * This aid with decoupling
 * 
 * @author Grym68
 *
 */
public interface ISerialize {

	/**
	 * Use this to serialize a list of objects type <T>  at a root
	 * location passed to the path parameter
	 * @param <T> - Generics for the type of object the list array
	 * stores
	 * @param array - the array of objects type <T> to serialize
	 * at path
	 */
	<T> void serialize(List<T> array, String path);

}