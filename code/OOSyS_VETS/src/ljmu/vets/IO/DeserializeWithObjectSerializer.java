package ljmu.vets.IO;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles Deserialization using Object serialization
 * 
 * @author Grym68
 *
 */
public class DeserializeWithObjectSerializer implements IDeserialize {
	private final String PATH = ".\\src\\data\\";
	
	/**
	 * Use this to deserialize data saved at a specified location
	 * as a List by passing the path to the location where the
	 * data is saved to the path parameter
	 * @param <T> - the type of object the list will hold
	 * @param path - the path to the data
	 * @return - returns a list of object <T>
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> deSerialize(String path) {
		ObjectInputStream ois;
		List<T> output = new ArrayList<T>();

		try {
			// might want to change this to path so that you can extract stuff from anywhere
			ois = new ObjectInputStream(new FileInputStream(path + "surgeries.ser"));

			// NOTE : Erasure Warning !
			output = (ArrayList<T>) ois.readObject();

			// ToDo : Finally ?
			ois.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
			
		}
		
		return output;
	}

	// this code might be useless because of the DeserializeWithJackson class
//	public <T> List<T> listWithJackson() {
//		//do code here
//		return null;
//	}

}
