package ljmu.vets.retrieve;

public interface IPetDataRetrieval<T> {

	/**
	 * Use this method to get a stringified and formated Pet
	 * Object
	 * @param pet - The pet Object you want to stringify
	 * @return - the Pet object as a formated string
	 */
	String toString(T pet);
}