package ljmu.vets.retrieve;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import ljmu.Factory;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;
import ljmu.vets.retrieve.lists.IRetrieveBookings;
import ljmu.vets.surgery.SurgeryValidator;

public class PetDataRetrieval implements IPetDataRetrieval<IPet>, IRetrieveBookings {

	@Override
	public String toString(IPet pet) {
		return pet.getClass().getSimpleName() + " >> " + pet.getName() + " "
				+ pet.getRegDate().format(DateTimeFormatter.ofPattern("dd MMM yy"));
	}

	/**
	 * Use this to get the a pet's next booking
	 * 
	 * @param bookings - The pet to get the booking from as an instance of Pet
	 * @return - returns a booking if successful or null if not
	 */
	@Override
	public IBooking getNextBooking(List<IBooking> bookings) {
		if (!Factory.surgeryValidator().isBookingsEmpty(bookings)) {
			for (IBooking output : bookings) {
				if (output.getWhen().isAfter(LocalDateTime.now())) {
					return output;
				}
			}
		}

		System.err.println("There are no bookings in the future");
		return null;

		/*
		 * I'm sorry Glyn, but this code was creating some problems with my
		 * implementation so I replaced it with my code P.S.: Yours is better :) return
		 * pet.getBookings().stream() .filter(o ->
		 * o.getWhen().isAfter(LocalDateTime.now())) .sorted(Comparator.comparing(o ->
		 * o.getWhen())) .findFirst().get();
		 */
	}
}
