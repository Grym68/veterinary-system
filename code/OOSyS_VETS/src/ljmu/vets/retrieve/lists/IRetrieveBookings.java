package ljmu.vets.retrieve.lists;

import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;

public interface IRetrieveBookings {

	/**
	 * Use this to get the a pet's next booking
	 * 
	 * @param bookings - The pet to get the booking from as an instance of Pet
	 * @return - returns a booking if successful or null if not
	 */
	IBooking getNextBooking(List<IBooking> bookings);

}