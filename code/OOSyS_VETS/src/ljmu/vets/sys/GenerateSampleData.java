package ljmu.vets.sys;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import ljmu.Factory;
import ljmu.vets.booking.Booking;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.cat.Cat;
import ljmu.vets.surgery.Surgery;
import ljmu.vets.surgery.iterfaces.ISurgery;

public class GenerateSampleData {
	
	/**
	 * Use this to populate surgeries with sample data by passing a <code>List<Surgery></code> 
	 * to surgeries 
	 * @param surgeries - this should be a list of tyoe Surgery
	 * @return - the updated list of surgeries
	 */
	public List<ISurgery> populateSurgeriesWithData(List<ISurgery> surgeries) {
		ISurgery surgery1 = Factory.makeSurgery();
		surgery1.setSurgery("SurgeryA");
		ISurgery surgery2 = Factory.makeSurgery();
		surgery2.setSurgery("SurgeryB");

		IPet pet1 = new Cat();
		pet1.setName("GlynH");
		pet1.setRegDate(LocalDate.of(2020, 11, 11));
		surgeries.set(0, Factory.surgeryGenerator().addPetsToSurgery(surgeries.get(0), pet1));

		IBooking booking1 = new Booking(); //DIP
		booking1.setRef("SurgeryA-REF1");
		booking1.setPet(pet1);
		booking1.setWhen(LocalDateTime.of(2022, 9, 9, 9, 33));
		booking1.setDuration(30);
		// problem here
		surgeries.set(0, Factory.surgeryGenerator().addBookingToSurgery(surgeries.get(0),booking1));

		IBooking booking2 = new Booking(); //DIP
		booking2.setRef("SurgeryA-REF2");
		booking2.setPet(pet1);
		booking2.setWhen(LocalDateTime.of(2023, 4, 4, 9, 22));
		booking2.setDuration(60);
		surgeries.set(1, Factory.surgeryGenerator().addBookingToSurgery(surgeries.get(0),booking2));

		IBooking booking3 = new Booking(); //DIP
		booking3.setRef("SurgeryA-REF3");
		booking3.setPet(pet1);
		booking3.setWhen(LocalDateTime.of(2025, 11, 11, 11, 00));
		booking3.setDuration(90);
		surgeries.set(2, Factory.surgeryGenerator().addBookingToSurgery(surgeries.get(0),booking3));
		
		return surgeries;
	}

}
