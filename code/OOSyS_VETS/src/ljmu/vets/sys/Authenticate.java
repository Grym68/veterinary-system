package ljmu.vets.sys;
import java.util.List;

import ljmu.vets.surgery.Surgery;
import ljmu.vets.surgery.iterfaces.ISurgery;

public class Authenticate {

	/**
	 * Use this to log a surgery on, it take a list of surgeries and outputs a surgery
	 * @param surgeries - the list of surgeries as <code>List<Surgery></code>
	 * @return - the surgery that is logged in now as <code>Surgery</code> object.
	 */
	public ISurgery logOn(List<ISurgery> surgeries) {
		// ToDo : Actually loggingOn :) ?
		ISurgery output = surgeries.get(0);
		
		return output;
	}
	
	public Surgery logOut() {
		return null;
	}
}
