package ljmu.vets.sys;
import java.util.ArrayList;
import java.util.List;

import ljmu.vets.surgery.Surgery;
import ljmu.vets.surgery.iterfaces.ISurgery;

public class Sys {
	protected final String PATH = ".\\src\\data\\";

	protected List<ISurgery> surgeries = new ArrayList<ISurgery>();
	private ISurgery surgery;
	// ToDo : Necessary ?
	public Sys() {
		// empty
	}

	public List<ISurgery> getSurgeries() {
		return surgeries;
	}

	public void setSurgeries(List<ISurgery> surgeries) {
		this.surgeries = surgeries;
	}

	public ISurgery getSurgery() {
		return surgery;
	}

	public void setSurgery(ISurgery surgery) {
		this.surgery = surgery;
	}
	
	public String getPATH() {
		return PATH;
	}
}
