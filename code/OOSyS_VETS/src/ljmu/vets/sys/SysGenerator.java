package ljmu.vets.sys;

import java.util.List;

import ljmu.Factory;
import ljmu.vets.surgery.Surgery;
import ljmu.vets.surgery.iterfaces.ISurgery;

public class SysGenerator {

	/**
	 * Use this to update the list of Surgeries in Sys with a given Surgery
	 * @param surgery - the Surgery object to pass to this function
	 * to update the list of Surgeries
	 * @param list - the list of Surgeries to update
	 * @return - the updated list of Surgeries
	 */
	public List<ISurgery> updateSurgeries(ISurgery surgery, List<ISurgery> list) {
		if(Factory.surgeryValidator().validate(surgery)) {
			for(int i = 0; i < list.size(); i++) {
				if(list.get(i).getSurgery().equals(surgery.getSurgery())) {
					list.set(i, surgery);
					return list;
				}
			}
		}
		System.err.println("Surgery couldn't be found, surgeries haven't been updated or saved");
		return list;
	}
}
