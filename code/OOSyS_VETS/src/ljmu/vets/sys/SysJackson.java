package ljmu.vets.sys;

import ljmu.Factory;
import ljmu.vets.IO.DeserializeWithJackson;
import ljmu.vets.IO.IDeserialize;
import ljmu.vets.IO.ISerialize;
import ljmu.vets.IO.SerializeWithJackson;

public class SysJackson extends Sys{
	public IDeserialize deserialize = new DeserializeWithJackson();
	public ISerialize serialize = new SerializeWithJackson();
	
	public SysJackson(){
		surgeries = deserialize.deSerialize(PATH);

//		 NOTE : Debugging !
		surgeries = Factory.generateSampleData().populateSurgeriesWithData(surgeries);
	}

}
