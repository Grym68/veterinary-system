package ljmu.vets.sys;
import java.util.Scanner;

import ljmu.Factory;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.PetDataCapture;
import ljmu.vets.pet.PetGenerator;
import ljmu.vets.surgery.iterfaces.ISurgery;

public class SurgeryMenus {
	static Scanner S = new Scanner(System.in);
	PetDataCapture petdatacapture = new PetDataCapture();
	static PetGenerator petGenerator = new PetGenerator();

	public ISurgery surgeryMenu(ISurgery surgery) {
			String choice = "";

			do {
				System.out.println("-- " + surgery.toString() 
				+ "'s SURGERY MENU --");
				System.out.println("1 : makePet");
				System.out.println("2 : getPetByName");
				System.out.println("3 : makeBooking");
				System.out.println("4 : getPetsNextBooking");
				System.out.println("5 : ToDo");
				System.out.println("Q : Quit");
				System.out.print("Pick : ");

				choice = S.nextLine().toUpperCase();

				switch (choice) {
				case "1": {
					surgery = Factory.surgeryGenerator().addPetsToSurgery(surgery, 
							petGenerator.generate());
					break;
				}
				case "2": {
					IPet pet = Factory.surgeryDataRetriever().petByName(surgery, 
							Factory.getPetDataCapture().captureName());
					System.out.println(pet.getDataRetrieval().toString(pet));
					break;
				}
				case "3": {
					surgery = Factory.surgeryGenerator().addBookingToSurgery(surgery, 
							Factory.getBookingDataCapture().capture(
									Factory.surgeryDataRetriever().petByName(surgery, 
											Factory.getPetDataCapture().captureName())));
					break;
				}
				case "4": {
					System.out.println(
							Factory.surgeryDataRetriever().getPetsNextBooking(surgery));
					break;
				}
				case "5": {
					// Testing
					System.out.println(surgery.getPets().get(0).getDataRetrieval().toString(surgery.getPets().get(0)));
					break;
				}
				}

			} while (!choice.equals("Q"));

			// NOTE : Logging Out !
			surgery = Factory.getAuthenticate().logOut();
			return surgery;
		}

}
