package ljmu.vets.notifiable;

public interface Notifiable {
	
	/**
	 * Notifies the recipient by formatting a passed message
	 * as a <code>String</code>
	 * @param s - the message to format and send to the recipient
	 */
	public void notify(String s);
}
