package ljmu.vets.surgery;

import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.surgery.iterfaces.ISurgery;
import ljmu.vets.surgery.iterfaces.ISurgeryValidator;

public class SurgeryValidator implements ISurgeryValidator {

	/**
	 * Use this to validate if a list of Bookings is empty
	 * @param bookings - the list of Bookings to validate
	 * @return - true if it is empty, or false if not
	 */
	@Override
	public boolean isBookingsEmpty(List<IBooking> bookings) {
		if(bookings.isEmpty()) {
//			throw new NullPointerException(); // change this to error message?
			System.err.println("There are no bookings");
			return true;
		}
		return false;
	}
	
	/**
	 * Use this to check if a Surgery is valid
	 * @param surgery - the surgery to check
	 * @return - true if it is valid, or false otherwise
	 */
	@Override
	public boolean validate(ISurgery surgery) {
		if(surgery == null) {
			System.err.println("This surgery is null");
			return false;
		}
		if(surgery.getSurgery().isBlank()) {
			System.err.println("This surgery's name is empty or blank");
			return false;
		}
		return true;
	}
}
