package ljmu.vets.surgery;

import java.time.format.DateTimeFormatter;

import ljmu.Factory;
import ljmu.vets.booking.BookingValidator;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingValidator;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.PetDataCapture;
import ljmu.vets.retrieve.PetDataRetrieval;
import ljmu.vets.retrieve.lists.IRetrieveBookings;
import ljmu.vets.surgery.iterfaces.ISurgery;
import ljmu.vets.surgery.iterfaces.ISurgeryDataRetrieval;

public class SurgeryDataRetrieval implements ISurgeryDataRetrieval {

	/**
	 * Use this method to get a Pet object by specifying the name 
	 * of the pet using a <code>String</code> to the <i>name</i> parameter
	 * 
	 * @param name - pass a <code>String</code> to this parameter
	 * @param surgery - the surgery in which to search for the pet
	 * as in instance of Surgery
	 * 
	 * @return Returns a <code>Pet</code> object if successful 
	 * or <code>null</code> if it fails
	 */
	@Override
	public IPet petByName(ISurgery surgery, String name) {
		// need to implement this better so that there is only one instance of this method :?

		// ToDo : Validate ?

		for (IPet pet : surgery.getPets()) {
			if (pet.getName().equals(name)) {
				return pet;
			}
		}
		return null;
	}
	
	/**
	 * Use this method to get a Booking by passing a <code>String</code> as 
	 * bookingReference parameter
	 * 
	 * @param bookingReference - a <code>String</code> reference to identify 
	 * the booking by 
	 * @return - a </code>Booking Object</code>
	 */
	@Override
	public IBooking getBookingByRef(String bookingReference, ISurgery surgery) {
		if (!Factory.surgeryValidator().isBookingsEmpty(surgery.getBookings()) && Factory.bookingValidator().validateReference(bookingReference)) {
			for (IBooking output : surgery.getBookings()) {
				if(output.getRef() == bookingReference) {
					return output;
				}
			}
		}
		return null;
	}
	
	/**
	 * Use this to Get the next booking of a pet
	 * @param surgery - the surgery to look into
	 * @return - returns the booking date as a formated 
	 * String if successfull or error messages otherwise
	 */
	@Override
	public String getPetsNextBooking(ISurgery surgery) {
		// ToDo : Validate ?
		IPet pet = petByName(surgery, new PetDataCapture().captureName());
		if (pet == null) {
			System.err.println("Pet is null!!");
		}

		IBooking booking = Factory.retrieveBookings().getNextBooking(pet.getBookings());
		if (booking == null)
			return "No bookings found! :(";
		
		return booking.getWhen().format(DateTimeFormatter.ofPattern("dd MMM yy HH:mm"));
	}
	
	/**
	 * Use this to get the type of object "<b>Surgery</b>" and the surgery id as a string.
	 * @param surgery - the surgery to get
	 * @return - the surgery as a formatted string
	 */
	@Override
	public String toString(ISurgery surgery) {
		return surgery.getClass().getSimpleName() + " >> " + surgery.getSurgery();
	}
}
