package ljmu.vets.surgery;

import java.util.List;

import ljmu.vets.booking.BookingValidator;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingValidator;
import ljmu.vets.pet.IPet;
import ljmu.vets.pet.PetGenerator;
import ljmu.vets.surgery.iterfaces.ISurgery;
import ljmu.vets.surgery.iterfaces.ISurgeryGenerator;

public class SurgeryGenerator implements ISurgeryGenerator {
	public IBookingValidator bookingValidator;
	public SurgeryGenerator() {
	 this.bookingValidator = new BookingValidator();
}
		
	/**
	 * Use this to add a Booking to the Surgery
	 * @param surgery - the Surgery to add to, must be of instance Surgery
	 * @param iBooking - the booking to add to the surgery, must be of instance Booking
	 * @return - the updated surgery object
	 */
	@Override
	public ISurgery addBookingToSurgery(ISurgery surgery, IBooking iBooking) {
		if(!bookingValidator.alreadyExists(iBooking, surgery.getBookings())
				&& bookingValidator.validate(iBooking)) {
			System.out.println("Booking Succesful! :)\n");
			
		// actually adding the booking to pet
			IPet pet = PetGenerator.addBookingToPet(iBooking.getPet(), iBooking); // this is super broken
			
			// overriding the pet in booking
			iBooking.setPet(pet); // this too
			
			// update the pets in surgery
			surgery.setPets(updatePets(surgery,pet));
			//adding the booking to surgery
			surgery.getBookings().add(iBooking);
			return surgery;
		}
		// Show message
		return surgery;
	}
	
	/**
	 * Use this to add a pet to surgery safely, it check if there
	 * is a pet in the surgery list of pets and replaces it, 
	 * if it already exits, or adds it if it doesn't
	 * @param surgery - the surgery to check for and update
	 * @param pet - the pet to add or update
	 * @return - returns the updated surgery object
	 */
	@Override
	public List<IPet> updatePets(ISurgery surgery, IPet pet) {
		for (int i = 0; i < surgery.getPets().size(); i++) {
			if(surgery.getPets().get(i).getName().equals(pet.getName())) {
				surgery.getPets().set(i, pet);
				return surgery.getPets();
			}
		}
		surgery = addPetsToSurgery(surgery,pet);
		return surgery.getPets();
	}
	
	/**
	 * Use this to add a pet to Surgery
	 * @param surgery - the surgery to add the pet to as instance of Surgery
	 * @param pet - the pet to add to the surgery as instance of Pet
	 * @return - the updated surgery
	 * 
	 */
	@Override
	public ISurgery addPetsToSurgery(ISurgery surgery, IPet pet) {
		surgery.getPets().add(pet);
		return surgery;
	}
}
