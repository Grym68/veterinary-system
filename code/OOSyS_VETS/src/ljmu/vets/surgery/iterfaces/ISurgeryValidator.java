package ljmu.vets.surgery.iterfaces;

import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;

public interface ISurgeryValidator {

	/**
	 * Use this to validate if a list of Bookings is empty
	 * @param bookings - the list of Bookings to validate
	 * @return - true if it is empty, or false if not
	 */
	boolean isBookingsEmpty(List<IBooking> bookings);

	/**
	 * Use this to check if a Surgery is valid
	 * @param surgery - the surgery to check
	 * @return - true if it is valid, or false otherwise
	 */
	boolean validate(ISurgery surgery);

}