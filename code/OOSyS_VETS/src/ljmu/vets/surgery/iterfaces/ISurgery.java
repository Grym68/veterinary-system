package ljmu.vets.surgery.iterfaces;

import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;

public interface ISurgery {

	ISurgeryDataRetrieval getSurgeryDataRetriever();

	void setSurgeryDataRetriever(ISurgeryDataRetrieval surgeryDataRetriever);

	/**
	 * Use this to get the surgery as a <code>String</code>
	 * @return - the id of the surgery as a String
	 */
	String getSurgery();

	/**
	 * Use this to set the id of a surgery by passing a 
	 * <code>String</code> to the surgery parameter of this method
	 * 
	 * @param surgery - expects a <code>String</code>
	 */
	void setSurgery(String surgery);

	List<IPet> getPets();

	List<IBooking> getBookings();

	void setPets(List<IPet> pets);

	void setBookings(List<IBooking> bookings);

}