package ljmu.vets.surgery.iterfaces;

import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;

public interface ISurgeryGenerator {

	/**
	 * Use this to add a Booking to the Surgery
	 * @param surgery - the Surgery to add to, must be of instance Surgery
	 * @param iBooking - the booking to add to the surgery, must be of instance Booking
	 * @return - the updated surgery object
	 */
	ISurgery addBookingToSurgery(ISurgery surgery, IBooking iBooking);

	/**
	 * Use this to add a pet to surgery safely, it check if there
	 * is a pet in the surgery list of pets and replaces it, 
	 * if it already exits, or adds it if it doesn't
	 * @param surgery - the surgery to check for and update
	 * @param pet - the pet to add or update
	 * @return - returns the updated surgery object
	 */
	List<IPet> updatePets(ISurgery surgery, IPet pet);

	/**
	 * Use this to add a pet to Surgery
	 * @param surgery - the surgery to add the pet to as instance of Surgery
	 * @param pet - the pet to add to the surgery as instance of Pet
	 * @return - the updated surgery
	 * 
	 */
	ISurgery addPetsToSurgery(ISurgery surgery, IPet pet);

}