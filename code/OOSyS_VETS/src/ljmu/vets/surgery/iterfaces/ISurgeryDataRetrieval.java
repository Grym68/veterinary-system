package ljmu.vets.surgery.iterfaces;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;

public interface ISurgeryDataRetrieval {

	/**
	 * Use this method to get a Pet object by specifying the name 
	 * of the pet using a <code>String</code> to the <i>name</i> parameter
	 * 
	 * @param name - pass a <code>String</code> to this parameter
	 * @param surgery - the surgery in which to search for the pet
	 * as in instance of Surgery
	 * 
	 * @return Returns a <code>Pet</code> object if successful 
	 * or <code>null</code> if it fails
	 */
	IPet petByName(ISurgery surgery, String name);

	/**
	 * Use this method to get a Booking by passing a <code>String</code> as 
	 * bookingReference parameter
	 * 
	 * @param bookingReference - a <code>String</code> reference to identify 
	 * the booking by 
	 * @return - a </code>Booking Object</code>
	 */
	IBooking getBookingByRef(String bookingReference, ISurgery surgery);

	/**
	 * Use this to Get the next booking of a pet
	 * @param surgery - the surgery to look into
	 * @return - returns the booking date as a formated 
	 * String if successfull or error messages otherwise
	 */
	String getPetsNextBooking(ISurgery surgery);

	/**
	 * Use this to get the type of object "<b>Surgery</b>" and the surgery id as a string.
	 * @param surgery - the surgery to get
	 * @return - the surgery as a formatted string
	 */
	String toString(ISurgery surgery);

}