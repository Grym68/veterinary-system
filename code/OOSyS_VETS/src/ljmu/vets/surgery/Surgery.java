package ljmu.vets.surgery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.pet.IPet;
import ljmu.vets.surgery.iterfaces.ISurgery;
import ljmu.vets.surgery.iterfaces.ISurgeryDataRetrieval;

/**
 * Surgery is used to define a Surgery
 * 
 * ToDo - make this SR
 * @author Grym68
 *  
 *
 */
public class Surgery implements Serializable, ISurgery {
	private String surgery;
	private List<IPet> pets = new ArrayList<IPet>();
	private List<IBooking> bookings = Collections.synchronizedList(new ArrayList<IBooking>());
	
	public ISurgeryDataRetrieval surgeryDataRetriever;


	/**
	 * Constructor for Surgery, requires:
	 * @param surgery - expects a <code>String</code> 
	 * to be passed to it.
	 */
	public Surgery() {
	this.surgeryDataRetriever	= new SurgeryDataRetrieval();
	}

	
	
	@Override
	public ISurgeryDataRetrieval getSurgeryDataRetriever() {
		return surgeryDataRetriever;
	}



	@Override
	public void setSurgeryDataRetriever(ISurgeryDataRetrieval surgeryDataRetriever) {
		this.surgeryDataRetriever = surgeryDataRetriever;
	}



	/**
	 * Use this to get the surgery as a <code>String</code>
	 * @return - the id of the surgery as a String
	 */
	@Override
	public String getSurgery() {
		return surgery;
	}

	/**
	 * Use this to set the id of a surgery by passing a 
	 * <code>String</code> to the surgery parameter of this method
	 * 
	 * @param surgery - expects a <code>String</code>
	 */
	@Override
	public void setSurgery(String surgery) {
		this.surgery = surgery;
	}

	@Override
	public List<IPet> getPets(){
		return this.pets;
	}
	
	@Override
	public List<IBooking> getBookings() {
		return bookings;
	}
	
	@Override
	public void setPets(List<IPet> pets) {
		this.pets = pets;
	}

	@Override
	public void setBookings(List<IBooking> bookings) {
		this.bookings = bookings;
	}

	/**
	 * Use this to make a Pet by providing a <code>Pet Object</code>
	 * to the pet parameter
	 * 
	 * @param pet - pass a <code>Pet Object</code> for this parameter
	 */
//	public void makePet(Pet pet) {
//		// ToDo : Validate ?
//		this.pets.add(pet);
//	}

	/*
	public void makePet(String name, LocalDate regDate) {
		// ToDo : Validate ?
	}
	*/

	/**
	 * Use this method to get a Pet object by specifying the name 
	 * of the pet using a <code>String</code> to the <i>name</i> parameter
	 * 
	 * @param name - pass a <code>String</code> to this parameter 
	 * @return Returns a <code>Pet</code> object if successful 
	 * or <code>null</code> if it fails
	 */
//	public Pet getPetByName(String name) {
//		// NOTE : Java SE 7 Code !
//		for (Pet p : this.pets) {
//			if (p.getName().equals(name)) {
//				return p;
//			}
//		}

		/* NOTE : Java SE 8 Code !
		Optional<Pet> p = this.pets.stream().filter(o -> o.getName().equals(name)).findAny();
		if (p.isPresent()) {
			return p.get();
		}
		*/

		// NOTE : No Match !
//		return null;
//	}

	/**
	 * use this method to make a new booking with the specified details
	 * using the reference, pet , date and duration of the booking
	 * 
	 * @param ref - the reference code of the Booking as a <code>String</code> type 
	 * @param pet - the pet object for the booking as a <code>Pet Object</code>
	 * @param when - the date on which the booking will happen <code>LocalDate</code> type
	 * @param duration - the amount in minutes as an <code> int </code> this
	 */
//	public void makeBooking(String ref, Pet pet, LocalDateTime when, Integer duration) {
//		// ToDo : Validate ?
//		for (Booking booking : this.bookings) {
//			if (booking.getRef() != ref && pet != null && booking.getWhen() != when && duration >= 15) {
////				this.bookings.add(new Booking(ref,pet,when,duration));
//				this.bookings.add(booking);
//			}
//		}
//	}
	
	/**
	 * Use this method to make a new booking by passing a <code>Booking Object</code>
	 * 
	 * @param booking - the Booking object to be added to the system as
	 *  as a <code>Booking Object</code>
	 */
//	public  void makeBooking(Booking booking) {
//		// ToDo : Validate ?
//		
//
//		// ToDo Two-Way Linking ?
//		booking.getPet().makeBooking(booking);
//	}

	

}
