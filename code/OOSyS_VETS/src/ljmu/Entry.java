package ljmu;
import ljmu.vets.EntryMenus;
import ljmu.vets.sys.Sys;

public class Entry {
	public static void main(String[] args) {
		// Sys sys = new Sys();
		// sys.entryMenu();

		// the best I could do is make it so that the user can control the 
		// type of serializing and deserializing by calling a different object.
		new EntryMenus().entryMenu(new Sys());
	}
}
