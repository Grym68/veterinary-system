package ljmu;

import ljmu.vets.IO.DeserializeWithObjectSerializer;
import ljmu.vets.IO.IDeserialize;
import ljmu.vets.IO.ISerialize;
import ljmu.vets.IO.SerializeWithObjectSerializer;
import ljmu.vets.booking.Booking;
import ljmu.vets.booking.BookingDataCapture;
import ljmu.vets.booking.BookingDataRetrieval;
import ljmu.vets.booking.BookingValidator;
import ljmu.vets.booking.interfaces.IBooking;
import ljmu.vets.booking.interfaces.IBookingDataCapture;
import ljmu.vets.booking.interfaces.IBookingDataRetriever;
import ljmu.vets.booking.interfaces.IBookingValidator;
import ljmu.vets.invoice.InvoiceCalculator;
import ljmu.vets.invoice.InvoiceToDocx;
import ljmu.vets.invoice.SaveToDocx;
import ljmu.vets.invoice.interfaces.Calculatable;
import ljmu.vets.invoice.interfaces.IInvoice;
import ljmu.vets.invoice.interfaces.ISaveInvoice;
import ljmu.vets.payable.medicine.Medicine;
import ljmu.vets.payable.medicine.PublicMedicine;
import ljmu.vets.payable.medicine.SurgeryMedicine;
import ljmu.vets.payable.medicine.interfaces.IMedicine;
import ljmu.vets.pet.IPetDataCapture;
import ljmu.vets.pet.PetDataCapture;
import ljmu.vets.pet.cat.Cat;
import ljmu.vets.pet.fish.Fish;
import ljmu.vets.pet.interfaces.IAquatic;
import ljmu.vets.pet.interfaces.IMamal;
import ljmu.vets.retrieve.PetDataRetrieval;
import ljmu.vets.retrieve.lists.IRetrieveBookings;
import ljmu.vets.surgery.Surgery;
import ljmu.vets.surgery.SurgeryDataRetrieval;
import ljmu.vets.surgery.SurgeryGenerator;
import ljmu.vets.surgery.SurgeryValidator;
import ljmu.vets.surgery.iterfaces.ISurgery;
import ljmu.vets.surgery.iterfaces.ISurgeryDataRetrieval;
import ljmu.vets.surgery.iterfaces.ISurgeryGenerator;
import ljmu.vets.surgery.iterfaces.ISurgeryValidator;
import ljmu.vets.sys.Authenticate;
import ljmu.vets.sys.GenerateSampleData;
import ljmu.vets.sys.SurgeryMenus;
import ljmu.vets.sys.SysGenerator;

public class Factory {

	public static IAquatic createFish() {
		IAquatic pet = new Fish();
		return pet;
	}
	
	public static IMamal createCat() {
		IMamal pet = new Cat();
		return pet;
	}
	public static IBooking createBooking() {
		IBooking booking = new Booking();
		return booking;
	}
	
	public static IBookingDataCapture getBookingDataCapture() {
		IBookingDataCapture captureBooking = new BookingDataCapture();
		return captureBooking;
	}
	
	public static IDeserialize getDeserializer() {
		IDeserialize deserialize = new DeserializeWithObjectSerializer();
		return deserialize;
	}
	
	public static ISerialize getSerializer() {
		ISerialize serialize = new SerializeWithObjectSerializer();
		return serialize;
	}
	
	public static IPetDataCapture getPetDataCapture() {
	IPetDataCapture dataCapture = new PetDataCapture();
	return dataCapture;
	}
	
	public static IBookingDataRetriever getBookindDataRetriever() {
		IBookingDataRetriever dataRetriever = new BookingDataRetrieval();
		return dataRetriever;
	}
	
	public static IBookingValidator bookingValidator() {
		IBookingValidator bookingValidator = new BookingValidator();
		return bookingValidator;
	}
	
	public static IInvoice getInvoice() {
		return new InvoiceToDocx(getInvoiceSaver(),getCalculatable());
	}
	
	public static ISaveInvoice getInvoiceSaver( ) {
		return new SaveToDocx();
	}
	
	public static Calculatable getCalculatable() {
		return new InvoiceCalculator();
	}
	
	public static IMedicine makeMedicine() {
		IMedicine medicine = new Medicine();
		return medicine;
	}
	
	public static IMedicine makePublicMedicine() {
		IMedicine publicMedicine = new PublicMedicine();
		return publicMedicine;
	}
	
	public static IMedicine makeSurgeryMedicine() {
		IMedicine surgeryMedicine = new SurgeryMedicine();
		return surgeryMedicine;
	}
	
	public static IPetDataCapture capturePetData() {
		IPetDataCapture petDataCapture = new PetDataCapture();
		return petDataCapture;
	}
	
	public static ISurgery makeSurgery() {
		ISurgery surgery = new Surgery();
		return surgery;
	}
	
	public static ISurgeryDataRetrieval surgeryDataRetriever() {
		ISurgeryDataRetrieval retriver = new SurgeryDataRetrieval();
		return retriver;
	}
	
	public static ISurgeryGenerator surgeryGenerator() {
		ISurgeryGenerator generator = new SurgeryGenerator();
		return generator;
	}
	
	public static ISurgeryValidator surgeryValidator() {
		ISurgeryValidator validator = new SurgeryValidator();
		return validator;
	}
	
	public static SurgeryMenus surgeryMenus() {
		return new SurgeryMenus();
	}
	
//IBookingValidator bookingValidator = new BookingValidator();
//IRetrieveBookings retrieveBookings = new PetDataRetrieval();
	
	public static IRetrieveBookings retrieveBookings() {
		IRetrieveBookings retriever = new PetDataRetrieval();
		return retriever;
	}
	
	public static GenerateSampleData generateSampleData() {
		GenerateSampleData generate = new GenerateSampleData();
		return generate;
	}
	
	
	public static Authenticate getAuthenticate() {
		return new Authenticate();
	}
	
	public static SysGenerator getSysGenerator() {
		return new SysGenerator();
	}
}
